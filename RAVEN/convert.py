# This script was made in order to convert Raven exported files    #
# to the ANIMAL-SPOT input, preserving the data corpus definition. #
# All we need is:
#
# - audio file in .wav extesnion
# - one .txt file with Raven timetable 

import pandas
import numpy as np
import os
import shutil
import argparse
import uuid
import datetime
from pydub import AudioSegment
import progressbar 
from pathlib import Path
import logging

# Finding current working directory
cwd = os.getcwd()
cwdPath = Path(cwd)

data_dir = "",
audio_filename = "",
aven_filename = ""
logger = logging.getLogger('training animal-spot')

config_data = dict()
log_level = "debug"
loglevels = ["debug", "error", "warning", "info"]

class COLOR: # https://stackoverflow.com/a/287944/11465149
    YELLOW    = '\033[93m'
    GREEN     = '\033[92m'
    RED       = '\033[91m'
    BOLD      = '\033[1m'
    ENDC      = '\033[0m'

widgets=[
    COLOR.RED + COLOR.BOLD, progressbar.Bar(left=' ', marker='━', right=' '), COLOR.ENDC,
]

def read_config(config_path):
        config_file = open(config_path, 'r')
        for element in config_file:
            if element[0] != "#" and element[0] != " " and element[0] != "" and element[0] != "\n":
                key = element.strip().split("=")[0]
                value = element.strip().split("=")[1]
                if value == "true":
                    config_data[key] = ""
                elif value == "false":
                    continue
                else:
                    config_data[key] = value
        logger.info("Config Data: " + str(config_data))

# Read the training config file
training_config_path = str(cwdPath.parent)+"/TRAINING/config"
read_config(training_config_path)


# Customize the target file extracted from Raven reading from Args
parser = argparse.ArgumentParser()
parser.add_argument("--classname", "-l", type=str, required=False, help="set the classname")
parser.add_argument("--name", "-n", type=str, required=False, help="set the name for the target directory")
parser.add_argument("--mode", "-m", type=str, required=False, help="set mode of Raven2Spot conversion, 'binary' or 'multiclass'")
parser.add_argument("--prefix", "-p", type=str, required=False, help="set the name for the dataset directory prefix")
parser.add_argument("--overwrite", "-o", type=bool, required=False, help="overwrite the content inside the target direcory in case it already exists, TRUE by default!)")                                        
parser.add_argument("--wav", "-w", type=str, required=False, help="set the audio .wav file full path")
parser.add_argument("--txt", "-t", type=str, required=False,help="set the info .txt file full path")
parser.add_argument("--couple", "-c", type=str, required=False, help="if info and audio files share the same basename, you can specify it without setting the -w and -t options")
args = parser.parse_args()

print("\n[###############################]")
print("[ Raven 2 Animal-Spot converter ]")
print("[###############################]\n")

if args.wav is not None and args.txt is not None:
    print("1) Reading data-preparation from 'RAVEN' specific filenames")
    raven_filename = args.wav #"16012023_greys_aviary"
    audio_filename = args.txt #"16012023_greys_aviary.wav"
elif args.couple is not None:
    print("1) Reading data-preparation from plain shared basename")
    raven_filename = cwd+"/"+args.couple+".txt"
    audio_filename = cwd+"/"+args.couple+".wav"
    print('> RAVEN FILE: '+raven_filename)
    print('> AUDIO FILE: '+audio_filename+"\n")
else:
    print("1) Reading data-preparation from 'RAVEN' directory")
    print("> TODO, not implemented yet, quitting…")
    quit()
    # TODO recursive multiple file parsing

# Simple loop algo made to excludes every odd index refereced to Spectrogam values
with open(f"{raven_filename}", 'r') as fp:
    lines = []
    for i, line in enumerate(fp):
        if(i == 0):
            lines.append(line)
        if(i != 0 and i%2 != 0):
            lines.append(line)

# Save the good lines inside another file that we'll call ${raven_filename}_fixed.txt
fixed_filename = raven_filename.split(".")[0]+"_fixed.txt"
fixed_file = open(fixed_filename, "w+")
fixed_file.writelines(lines)
fixed_file.close()

#Read the fixed file again to extrapulate and convert the time values from secs to mills

print("2) Extracting data…\n\ncounting the classnames")
df = pandas.read_csv(fixed_filename,sep="\t")
df['Begin Time (s)'] = df['Begin Time (s)'].apply(lambda x: x*1000)
df['Delta Time (s)'] = df['Delta Time (s)'].apply(lambda x: x*1000)
df = df.rename(columns={'Annotation':'Note','Selection': '#N','Delta Time (s)':'Delta Time (ms)','Begin Time (s)':'Begin Time (ms)'})

# grouping data columns by Annotation to get the Classname
onlyClassname = df.filter(['#N','Note'], axis=1)
print(onlyClassname.groupby('Note').count().rename(columns={'#N':'n'}))
print("\n")
print("3) Create the dataset using labels")

# Delete the fixed.txt file
if os.path.exists(fixed_filename):
  os.remove(fixed_filename)
else:
  print("The file '"+fixed_filename+"' does not exist")

# progress bar initialization
# bar = progressbar.ProgressBar(maxval=df.size, \
#    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])

if args.overwrite is not None and args.overwrite is False:
    print("> Overwrite: disabled ✕")
else:
    if args.overwrite is not None and args.overwrite is True:
        print("> Overwrite: enable ✓")
    # Create the folder that will contain the future dataset
    labeldir = "Directory creation"
    try:
        dirname = ""
        if args.name is not None:
            dirname = args.name.upper()
            data_dir = str(cwdPath.parent)+"/"+dirname
            os.mkdir(data_dir)
        else:
            data_dir = config_data['data_dir']
        
    except OSError as e:
        print ("%s: Failed: %s \nPath: %s" %(labeldir,str(e).split(":")[0],data_dir) )
    else:
        print ("%s: Successful ✓\nPath: %s " %(labeldir,data_dir) )

# Reiterate over pandas dataframe
print("\n4) Dataset creation…")
with progressbar.ProgressBar(max_value=len(df.index)) as bar:
    for index, row in df.iterrows():
        classname = row['Note']
        labelinfo = ""
        
        if(classname != 'noise'):
            classname = 'target'
            if(args.classname is not None):
                classname = args.classname
            labelinfo = row['Note']
        if(classname == 'noise'):
            labelinfo = 'lab'
        
        classname = classname.replace(" ", "").lower()
        labelinfo = labelinfo.replace(" ", "").lower()
            
        id = row['#N']
        year = 2023
        tapename = raven_filename.split(".")[0].split("/")[-1].upper()
        starttimes = "{0:.0f}".format(row['Begin Time (ms)'])
        endtimes = "{0:.0f}".format(row['Begin Time (ms)']+row['Delta Time (ms)'])

        copied_filename = classname+"-"+labelinfo+("_N_"+str(year)+"_"+tapename+"_"+str(starttimes)+"_"+str(endtimes)).upper()+".wav"
        
        newfilename = data_dir+"/"+copied_filename
        
        # Crop the long audio in small samples
        # audio rec
        rec = AudioSegment.from_wav(audio_filename)
        millisec_total_duration = rec.duration_seconds * 1000 
        #print("wrong total duration is: "+str(millisec_total_duration))

        start_offset = millisec_total_duration - row['Begin Time (ms)']
        #print("fixed begin time is: "+str(start_offset))
        
        end_offset = row['Delta Time (ms)']
        #print("fixed end time is: "+str(end_offset))

        cut_start_rec = rec[-start_offset:]
        cut_end_rec = cut_start_rec[:end_offset]

        millisec_total_duration = cut_end_rec.duration_seconds * 1000
        #print("fixed total duration is: "+str(millisec_total_duration))
        
        cut_end_rec.export(newfilename, format="wav")

        #old copying
        #shutil.copy(audio_filename,newfilename)
        bar.update(index)
        index = index+1
bar.finish()
print("\n5) Rebuilding index:  ✓")
files = []
for file in cwdPath.parent.joinpath("DATA").iterdir():
    if(str(file).split(".")[1].upper()== "WAV"):
        files.append(file)
print("files are "+str(len(files)))
filename_index = 0
for file in cwdPath.parent.joinpath("DATA").iterdir():
    if(str(file).split(".")[1].upper()== "WAV"):
        filename = str(file)
        filename_prefix = filename.split("_")[0]
        filename_suffix = filename.split("_")[2]
        #filename.replace
        filename_complete = ""
        for idx,part in filename.split["_"]:
            if(idx != 1):
                filename_complete=filename_complete+"_"+part
            else:
                filename_complete=filename_complete+"_"+idx
        file.rename(filename_complete)
print("\n6) Raven Dataset conversion: Done ✓")