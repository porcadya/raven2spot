FROM pytorch/pytorch:latest

RUN pip install torch torchvision librosa tensorboardX matplotlib pillow six opencv-python soundfile scikit-image torchaudio pandas resampy

RUN mkdir /animal-spot

WORKDIR /animal-spot

COPY . ./

RUN ["chmod", "+x", "./entrypoint.sh"]

ENTRYPOINT ["./entrypoint.sh"]