
# RAVEN2SPOT
#### This is [ANIMAL-SPOT](https://github.com/ChristianBergler/ANIMAL-SPOT) fork that helps you integrate the exported RAVEN files inside the training dataset.
 ## RAVEN export standard VS RAVEN training input data

raven2spot split and crop the single .wav in smaller .wav files with the standard filename pattern defined in standard 
- `Filename Template: CLASSNAME-LABELINFO_ID_YEAR_TAPENAME_STARTTIMEMS_ENDTIMEMS.wav` 

In the table below you can see the matching fields taken from the export and used to build each filename in the dataset. 
According to the same standard we rebuild the index each time a new import session is done, so that each file has an unique ID

- for example: target-grey_1_2023_16012023_001_666.wav

|RAVEN OUTPUT NAME|ANIMAL-SPOT TRAINING INPUT|MATCH
|--|--|--|
|Selection||❌|
|View||❌|
|Channel||❌|
|Begin Time (s)|STARTTIME (ms)|✅|
|End Time (s)|STARTTIME (ms)|✅|
|Low freq (Hz)||❌|
|High freq (Hz)||❌|
|ΔTime (s)||❌|
|Δ freq (Hz)||❌|
|Avg Power density (dB FS/Hz)||❌|
|Annotation|LABELINFO|✅|


Sound files (.wav) exported via [Raven](https://ravensoundsoftware.com/) can have labels defined in some info plain .txt files. Those files are gnerally exported in couples when labeles exists, and this is useful to make a correct classification in Neural Newtowrks.

### just run `python3 convert.py `!

The `convert.py` script build the DATASET folder and let you start the training without think about the pattern

#### Arguments

-  `--name` or `-n`  _the **n**ame for the target directory_
-  `--mode` or `-m`  _conversion **m**ode, 'binary' or 'multiclass'_
-  `--target` or `-t`  _**t**arget, the dataset directory's prefix_
-  `--overwrite` or `-o`  _**o**verwrite the content inside the target direcory in case it already exists, TRUE by default!)_
-  `--wav` or `-w`  _set the audio .**w**av file full path_
-  `--txt` or `-t` set the info .**t**xt file full path_
-  `--couple` or `-c`  _if info and audio files share the same basename, you can specify the **c**ouple in one-shop, without setting the -w and -t options_


### IMPORT MODE

This feature offers a import algorythm in order to create a correct dataset for the Traning session in ANIMAL-SPOT.
  
#### - RAVEN EXPORT: use a couple of .txt & .wav file

if the RAVEN exported files are `Lizzy_room2_Nina_room3_cages_22.01.2023.wav` and `Lizzy_room2_Nina_room3_cages_22.01.2023.txt`
we can use the command argument as below:
- `python3 convert.py --couple Lizzy_room2_Nina_room3_cages_22.01.2023`
without typing the extension

```mermaid
graph LR
A(Read files) --> D
D --> E
E(Labeling the files)
D(Split the .wav in multiple .wav)
E --> F[Reindexing IDs]
F --> G(Copying the files in the DATASET)
```

### 1) SHARED BASENAME
If the two Raven files share the same basename you have to use the `--couple`/`-c` option:
1) place the files in the `RAVEN` directory
2) run the script as below:

-  `python3 convert.py --couple PLAIN_FILENAME_WITHOUT_EXTENSION`
### 2) USE .wav and .txt WITH SPECIFIC ABSOLUTE PATHS
If those have different basename and/or different paths you can use the same script passing the `--wav`/`-w` and the `--txt`/`-t` argument:

-  `python3 convert.py --wav ABSOLUTE/PATH/TO/WAV/FILE.wav --txt ABSOLUTE/PATH/TO/TXT/FILE.txt`

## USE WITH AUTOMATIC MULTIPLE FILES

This is a TODO sub-feature